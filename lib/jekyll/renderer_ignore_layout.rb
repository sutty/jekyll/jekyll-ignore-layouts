# frozen_string_literal: true

module Jekyll
  module RendererIgnoreLayout
    def self.included(base)
      base.class_eval do
        alias :original_invalid_layout? :invalid_layout?

        # Only write the document when its layout is not ignored
        def invalid_layout?(layout)
          return false if site.ignored_layout?(document.data['layout'])

          original_invalid_layout? layout
        end
      end
    end
  end
end

Jekyll::Renderer.include Jekyll::RendererIgnoreLayout
