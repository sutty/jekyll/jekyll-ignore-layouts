# frozen_string_literal: true

module Jekyll
  module SiteIgnoreLayout
    def self.included(base)
      base.class_eval do
        # Determine if a layout is ignored and cache the result
        def ignored_layout?(layout)
          return false unless config['ignored_layouts']

          unless config['ignored_layouts'].is_a? Array
            raise Jekyll::Errors::InvalidConfigurationError, "`ignored_layouts` must be an Array"
          end

          @ignored_layout ||= {}

          # We could use ||= but it runs everytime when inclusion
          # returns false.
          if @ignored_layout.key? layout
            @ignored_layout[layout]
          else
            @ignored_layout[layout] = config['ignored_layouts'].include?(layout)
          end
        end
      end
    end
  end
end

Jekyll::Site.include Jekyll::SiteIgnoreLayout
