# jekyll-ignore-layouts

In Sutty's workflow, some articles are meant to be included on other
pages, so they don't require to be rendered as individual files/URLs.

With this plugin, when an article's layout is ignored, the article is
available on liquid templates but doesn't get written to disk.

## Installation

Add this line to your site's Gemfile:

```ruby
gem 'jekyll-ignore-layouts'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-ignore-layouts

## Usage

Add the plugin to your `_config.yml` and configure it:

```yaml
plugins:
- jekyll-ignore-layouts
ignored_layouts:
- menu
```

`ignored_layouts` is an array of layouts that don't required to be
written to disk.

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/jekyll/jekyll-ignore-layouts>. This project is
intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

If you like our plugins, [please consider
donating](https://donaciones.sutty.nl/en/)!

## License

The gem is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the jekyll-ignore-layouts project’s codebases,
issue trackers, chat rooms and mailing lists is expected to follow the
[code of conduct](https://sutty.nl/en/code-of-conduct/).
