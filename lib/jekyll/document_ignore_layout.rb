# frozen_string_literal: true

module Jekyll
  module DocumentIgnoreLayout
    def self.included(base)
      base.class_eval do
        alias :original_write? :write?

        # Only write the document when its layout is not ignored
        def write?
          return false if site.ignored_layout?(data['layout'])

          original_write?
        end
      end
    end
  end
end

Jekyll::Document.include Jekyll::DocumentIgnoreLayout
