# frozen_string_literal: true

module Jekyll
  module DoctorIgnoreLayout
    def self.included(base)
      base.class_eval do
        class << self
          private

          alias :original_allow_used_permalink? :allow_used_permalink?

          def allow_used_permalink?(item)
            return true if item.respond_to?(:site) &&
              item.respond_to?(:data) &&
              item.site.ignored_layout?(item.data['layout'])

            original_allow_used_permalink? item
          end
        end
      end
    end
  end
end

Jekyll::Commands::Doctor.include Jekyll::DoctorIgnoreLayout
